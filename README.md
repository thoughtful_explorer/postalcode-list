# 25-mile Radius Postal Code List
The purpose of this project is to generate a strategic list of postal codes (in this case, US ZIP codes) such that if the minimum possible number of postal codes were given a roughly 25 mile surrounding radius, the entire country would be covered with minimal or no likely gaps. The ultimate purpose is to generate a minimum list of postal codes that would effectively cover a whole country (in this case, just the US lower 48) if used to query mapping APIs that often require the submission of a zip code and return locations within an arbitrary radius (20-25 miles is a typical option in such APIs). Essentially, its intended purpose is for scraping mapping APIs to collect (nearly) nationwide map data.

## Additional Options
- This method can of course be used with any arbitrary radius distance, not just 25 miles
- This method could also easily be extended to encompass Hawai'i, Alaska, Puerto Rico, et al; or even other countries.

Note: I am not a professional geographer. There may be (and probably are) far better approaches to which I remain open to suggestion - this is good enough to get the ball across the goal line for my own purposes.

# Approach
The approach was very much inspired by [this video](https://inv.zzls.xyz/watch?v=zG-5TB5O9og). To arrive at a rough result, a 25 mile buffer radius around the geographic shape of each postal code is a relatively intensive computational proposition, and was therefore *not* chosen. (This is also unlikely how mapping API distance radius algorithms work). Instead, [QGIS](https://qgis.org) desktop GIS software was used to generate a list of postal codes by following to the following instructions:

1. Acquire federal boundary polygon data (the ["United States Outline" shapefile](https://www.census.gov/geographies/mapping-files/time-series/geo/cartographic-boundary.html), for instance)
1. Remove everything but the lower 48 shape
1. Make sure to zoom the QGIS window out to at least one zoomlevel out from the extents of the lower 48 shape
1. Using the [MMQGIS](https://plugins.qgis.org/plugins/mmqgis/) plugin for QGIS, run the "Create Grid Layer" tool to create a grid of tiled hexagons
    1. Choose hexagons as the geometry type
    1. Assuming meters are used as the default project scale units, and the CRS is EPSG:3857, choose "Project Units" and use 80467 (this is roughly 50 miles) as the X spacing -- the Y spacing will be calculated and entered automatically
    1. Because I found the "Layer Extent" and "Current Window" to be unreliable in this plugin, choose "Custom Area" and manually specify chosen top left and bottom right coordinates for a rectangle that adequately covers the whole shape (I used Left X: -14145967 | Top Y: 6535720 | Right X: -7275221 | Bottom Y: 2733759)
    1. Choose any output file name - this is not so important (US_hexagons.shp, for instance)
    1. When applied correctly, this will draw a grid of 50-mile wide hexagons as a layer on top of the continental US layer
1. Apply the Select By Location (Vector > Research Tools) tool:
    1. Select features from the hexagons layer where the features *intersect* by comparing the features from the US lower 48 layer, creating a new selection
    1. Toggle the editing mode on the hexagon layer, open the attribute table and invert the selection (Ctrl+R), and choose Delete Selected to remove all the hexagons that do not touch the US lower 48 polygon
    1. Toggle the editing mode again and save the edits
1. Apply the Centroids tool (Vector > Geometry Tools) on the hexagon layer, creating another layer of points - this layer is a rectangular grid of points, all about 50 miles away from each other, located at the center of each hexagon
1. Add a postal code ([US ZIP code boundaries](https://hub.arcgis.com/datasets/esri::usa-zip-code-boundaries/explore)) boundary polygon layer 
1. Although choosing the Select by Location tool would theoretically work here, I had more success using the **Extract by Location** tool:
    1. Extract features from the postal code boundaries layer where the features *intersect* by comparing to the features from the previously created centroid points layer
    1. In the Advanced settings, choose the "Skip (Ignore) Features with Invalid Geometries" setting for the postal code layer, as some of the centroid points might not overlap a postal code boundary - this rightfully ignores points placed in large bodies of water that of course do not have postal codes
    1. Expect this process to take a bit of time (about 10 minutes)
1. The resulting layer is the desired product - it can be exported as a CSV file, with all the other metadata pared away to result in a list of only the postal codes using any common spreadsheet editor
    1. PROTIP: remember to treat the postal code column as text instead of numerical to avoid snipping leading zeros, as they are used in US ZIP codes

The resulting CSV file [25milepostalcodes.csv](25milepostalcodes.csv) from this process has been included in this project for convenience. This reduces the number of required postal code queries from the total of 32268 distinct US postal codes as of this writing (this includes all 50 states and territories), down to 1661.

## Caveats
One of the clear weaknesses of using this method -- at least specifically with zip codes -- is when the placement of one of the centroids ends up in water. The aforementioned zip code polygon data acquired for this project does not extend zip code areas into large bodies of water. If the matrix of centroids is forced to directly **intersect** with zip codes and trim off everything else, then it is possible for a point to be missing in coastal areas that might otherwise be well-populated and likely rich with geographic API data. 
![Map of ATM data points showing clearly missing data](img/missing_points.png "Some ATM locations are clearly missing")

The above image shows a map during the creation of the [Allpoint ATM locations project](https://gitlab.com/thoughtful_explorer/allpoint-map) that demonstrates the weakness of this project's approach. To remedy the issue in this particular case, I chose to manually place points on the nearest piece of land that also achieves a general consistency in point spacing. Alternative and/or more precise non-manual solutions could be interesting for more rigorous use cases, and certainly underscores the complexity of this type of geographic/mathematic problem solving.
